class CreateEchoProducerApis < ActiveRecord::Migration
  def change
    create_table :echo_producer_apis do |t|
      t.string :producer_id
      t.string :quantity
      t.string :value
      t.string :nreduction
      t.string :preduction
      t.date :startDate
      t.text :image
      t.string :feet
      t.datetime :approvalDate
      t.string :acres
      t.string :practiceTypeID
      t.string :practiceTypeName
      t.string :feetRequired
      t.string :stateID
      t.string :stateName
      t.string :low_till

      t.timestamps null: false
    end
  end
end
