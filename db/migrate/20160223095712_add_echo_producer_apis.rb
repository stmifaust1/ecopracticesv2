class AddEchoProducerApis < ActiveRecord::Migration
  def change
   change_table :echo_producer_apis do |t|
      t.string :minnreduction 
      t.string :maxnreduction 
      t.string :nh4nreduction 
      t.string :no3nreduction 
      t.string :minpreduction 
      t.string :maxpreduction 
      t.string :nh4preduction 
      t.string :no3preduction
    end
  end
end
