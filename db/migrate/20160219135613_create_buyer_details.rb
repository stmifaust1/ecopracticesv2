class CreateBuyerDetails < ActiveRecord::Migration
  def change
    create_table :buyer_details do |t|
      t.string :company_name
      t.string :company_contact_name
      t.string :company_contact_email
      t.string :company_contact_phone
      t.string :company_contact_address
      t.string :company_contact_city
      t.string :company_contact_state
      t.string :company_contact_zip
      t.string :company_logo
      t.string :company_header
      t.string :password
      t.string :confirm_password
      t.datetime :purchase_date
      t.boolean :is_active

      t.timestamps null: false
    end
  end
end
