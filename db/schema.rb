# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160223095712) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "buyer_details", force: :cascade do |t|
    t.string   "company_name",            limit: 255
    t.string   "company_contact_name",    limit: 255
    t.string   "company_contact_email",   limit: 255
    t.string   "company_contact_phone",   limit: 255
    t.string   "company_contact_address", limit: 255
    t.string   "company_contact_city",    limit: 255
    t.string   "company_contact_state",   limit: 255
    t.string   "company_contact_zip",     limit: 255
    t.string   "company_logo",            limit: 255
    t.string   "company_header",          limit: 255
    t.string   "password",                limit: 255
    t.string   "confirm_password",        limit: 255
    t.datetime "purchase_date"
    t.boolean  "is_active",               limit: 1
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "echo_producer_apis", force: :cascade do |t|
    t.string   "producer_id",      limit: 255
    t.string   "quantity",         limit: 255
    t.string   "value",            limit: 255
    t.string   "nreduction",       limit: 255
    t.string   "preduction",       limit: 255
    t.date     "startDate"
    t.text     "image",            limit: 65535
    t.string   "feet",             limit: 255
    t.datetime "approvalDate"
    t.string   "acres",            limit: 255
    t.string   "practiceTypeID",   limit: 255
    t.string   "practiceTypeName", limit: 255
    t.string   "feetRequired",     limit: 255
    t.string   "stateID",          limit: 255
    t.string   "stateName",        limit: 255
    t.string   "low_till",         limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "minnreduction",    limit: 255
    t.string   "maxnreduction",    limit: 255
    t.string   "nh4nreduction",    limit: 255
    t.string   "no3nreduction",    limit: 255
    t.string   "minpreduction",    limit: 255
    t.string   "maxpreduction",    limit: 255
    t.string   "nh4preduction",    limit: 255
    t.string   "no3preduction",    limit: 255
  end

end
