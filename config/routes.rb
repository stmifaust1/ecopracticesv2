Rails.application.routes.draw do

  get 'buyer_info/index'
  get 'buyer_info/intro'
  get 'buyer_info/effects'

  get 'marketing/index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'buy_eco/controller'

  get 'track_tags/index'

  get 'checkout/index'
  get 'checkout/item'

  get 'show_tag/index'

  get 'how_it_works/index'
    
  get 'who_we_are/index'

  get 'buy_tags/index'
  get 'buy_tags/import_producer_api'
  get 'buy_tags/search_results'
  get 'buy_tags/buy'
  get 'buy_tags/check_echotag'
  get 'buy_tags/echotag_enq'

  match "buy_eco/index" => "buy_eco#index", via: [:get, :post]
  match "produce_tags/index" => "produce_tags#index", via: [:get, :post]

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  resources :contacts, only: [:new, :create], path: '/contact', path_names: { new: ''}

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
