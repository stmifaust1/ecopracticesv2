# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( *.js *.scss *.css.scss *.PNG *.png *.jpg *.jpeg *.gif *.woff *.ttf *.svg *.eot)
Rails.application.config.assets.precompile += %w( down-btn.png )
Rails.application.config.assets.precompile += %w( buyer-bg.png )
Rails.application.config.assets.precompile += %w( p-o-p.png )
Rails.application.config.assets.precompile += %w( new-tweets.png )
Rails.application.config.assets.precompile += %w( stacked-logo.png )
Rails.application.config.assets.precompile += %w( custom-marketing-bg.png )
Rails.application.config.assets.precompile += %w( round-building.png )
Rails.application.config.assets.precompile += %w( round-farmers.png )
Rails.application.config.assets.precompile += %w( round-pig.png )
Rails.application.config.assets.precompile += %w( round-phone.png )
Rails.application.config.assets.precompile += %w( round-stock.png )
Rails.application.config.assets.precompile += %w( round-statement.png )
Rails.application.config.assets.precompile += %w( round-price.png )
Rails.application.config.assets.precompile += %w( eco-example-screen.png )
Rails.application.config.assets.precompile += %w( lg-bathtub.png )
Rails.application.config.assets.precompile += %w( lg-car.png )