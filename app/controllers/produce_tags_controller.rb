class ProduceTagsController < ApplicationController
  
  add_flash_types :success, :warning, :danger, :info
  
  def index
  	if request.post?
  		name = params[:name]
  		email = params[:email]
  		phone = params[:phone]
  		subject = params[:subject]
  		message = params[:message]
  		
      check_captcha(params, request) if request
      if @recaptcha_response['success'] != true
        flash.now[:danger] = "<h4>CAPTCHA mismatch, try again</h4>"
      else
    		if EchotagMailer.ecotag_email(name, email, phone, subject, message).deliver  			
    			flash.now[:success] = "<h3>Success</h3><p>Thank you for your interest in producing Ecotags. Your information has been received and someone will be contacting you shortly.</p>"
    		end
      end
  	end
  end

  def check_captcha(params, request)
    secret = "6LfutQ0UAAAAAAZv9Tq0SUCBnUFQrR8r8QhvzzQO"
    uri = URI.parse("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + params['g-recaptcha-response'] + "")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request_url = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request_url)
    @recaptcha_response =  JSON.parse(response.body)    
  end
end
