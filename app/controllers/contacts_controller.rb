class ContactsController < ApplicationController

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])

    if @contact.valid?
      ContactMailer.contact_email(@contact).deliver
      render json: @contact, status: :ok
      #redirect_to root_path, notice: "Your email has been sent. Thank you."
    else
      render "welcome/index"
    end
  end

end
