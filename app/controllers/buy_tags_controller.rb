require 'open-uri'
require 'openssl'
require 'mini_magick'

class BuyTagsController < ApplicationController
  
  def index
    if params[:impact] && params[:state] && params[:sector] 
        @producer_data = EchoProducerApi.where("stateName = ? AND practiceTypeName = ? AND impact LIKE ?",params[:state], params[:sector], "%#{params[:impact]}%").paginate(:page => params[:page] , :per_page => 10)
    
    elsif params[:state] && params[:impact]
        @producer_data = EchoProducerApi.where("stateName = ? AND impact LIKE ? ", params[:state], "%#{params[:impact]}%").paginate(:page => params[:page] , :per_page => 10)

    elsif params[:sector] && params[:impact]
        @producer_data = EchoProducerApi.where("practiceTypeName = ? AND impact LIKE ? ", params[:sector], "%#{params[:impact]}%").paginate(:page => params[:page] , :per_page => 10)
    
    elsif params[:sector] && params[:state]
        @producer_data = EchoProducerApi.where("practiceTypeName = ? AND stateName = ? ", params[:sector], params[:state]).paginate(:page => params[:page] , :per_page => 10)

     elsif params[:impact]
        @producer_data = EchoProducerApi.where("impact LIKE ?","%#{params[:impact]}%").paginate(:page => params[:page] , :per_page => 10)
    
    elsif params[:state]
        @producer_data = EchoProducerApi.where("stateName = ?", params[:state]).paginate(:page => params[:page] , :per_page => 10)

    elsif params[:sector]
        @producer_data = EchoProducerApi.where("practiceTypeName = ?", params[:sector]).paginate(:page => params[:page] , :per_page => 10)

    else       
        #@producer_data = EchoProducerApi.joins(:echo_practice_api).select("echo_practice_apis.*,echo_producer_apis.*").paginate(:page => params[:page] , :per_page => 10)
        @producer_data = EchoProducerApi.paginate(:page => params[:page] , :per_page => 10)
    end
  end

  def import_producer_api
    data_main  = JSON.parse(open('https://producer.ecocredits.me/api/v-1-3/credits/get.php?token=yi9u/BzRryjjTAafHh4ljkUvDzAMwPhQ7MTCFb2UNuE=').read)
    @new_record = JSON.parse(data_main['data'])    

    @new_record.each do |eco_value|
      eco_producer = EchoProducerApi.new      
      eco_producer_update = EchoProducerApi.find_by(producer_id: eco_value['id'])

      url = "https://producer.ecocredits.me/api/v-1-3/credits/getImage.php?token=yi9u/BzRryjjTAafHh4ljkUvDzAMwPhQ7MTCFb2UNuE=&id=#{eco_value['id']}"
      image_data = JSON.parse(open(url).read)

      if EchoProducerApi.exists?(:producer_id => eco_value['id'])
        eco_producer_update.value = eco_value['value']
        if eco_value['quantity']
          eco_producer_update.quantity = eco_value['quantity']
        else
          eco_producer_update.quantity = 0
        end
          eco_producer_update.startDate = eco_value['startDate']
          eco_producer_update.nreduction = eco_value['nreductionavg'] 
          eco_producer_update.preduction = eco_value['preductionavg']
          eco_producer_update.feet = eco_value['feet']
          eco_producer_update.approvalDate = eco_value['approvalDate']
          eco_producer_update.acres = eco_value['acres']
          eco_producer_update.practiceTypeID = eco_value['practiceTypeID']
          eco_producer_update.practiceTypeName = eco_value['practiceTypeName']
          eco_producer_update.feetRequired = eco_value['feetRequired']
          eco_producer_update.stateID = eco_value['stateID']
          eco_producer_update.stateName = eco_value['stateName']
          eco_producer_update.minnreduction = eco_value['nreductionmin']
          eco_producer_update.maxnreduction = eco_value['nreductionmax']
          eco_producer_update.nh4nreduction = eco_value['nreductionnh4']
          eco_producer_update.no3nreduction = eco_value['nreductionno3']
          eco_producer_update.minpreduction = eco_value['preductionmin']
          eco_producer_update.maxpreduction = eco_value['preductionmax']
          eco_producer_update.nh4preduction = eco_value['preductiontotalp']
          eco_producer_update.no3preduction = eco_value['preductionsolublep']

          if !eco_producer_update.image
            if image_data['status'] == 'success'
              image_url = image_data['data']
              file = MiniMagick::Image.open(URI.decode(image_url['imageurl']))
              save_to_local = file.write  "public/assets/images/#{eco_value['id']}.#{file.type}"
              eco_producer_update.image = "#{eco_value['id']}.#{file.type}"
            end
          end
          if eco_value['practiceTypeID'] == 1
            eco_producer_update.impact = 'Water Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 2 || eco_value['practiceTypeID'] == 3
            eco_producer_update.impact = 'Water Quality,Air Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 4 || eco_value['practiceTypeID'] == 5
            eco_producer_update.impact = 'Water Quality'
          elsif eco_value['practiceTypeID'] == 6
            eco_producer_update.impact = 'Water Quality,Erosion Control,Carbon Reduction'
          elsif eco_value['practiceTypeID'] == 7
            eco_producer_update.impact = 'Water Quality,Air Quality'
          elsif eco_value['practiceTypeID'] == 8
            eco_producer_update.impact = 'Water Quality'    
          end
          eco_producer_update.save
      else
          eco_producer.producer_id = eco_value['id']
          eco_producer.value = eco_value['value']
          if eco_value['quantity']
            eco_producer.quantity = eco_value['quantity']
          else
            eco_producer.quantity = 0
          end
          eco_producer.startDate = eco_value['startDate']        
          eco_producer.nreduction = eco_value['nreductionavg']
          eco_producer.preduction = eco_value['preductionavg']
          eco_producer.feet = eco_value['feet']
          eco_producer.approvalDate = eco_value['approvalDate']
          eco_producer.acres = eco_value['acres']
          eco_producer.practiceTypeID = eco_value['practiceTypeID']
          eco_producer.practiceTypeName = eco_value['practiceTypeName']
          eco_producer.feetRequired = eco_value['feetRequired']
          eco_producer.stateID = eco_value['stateID']
          eco_producer.stateName = eco_value['stateName']
          eco_producer.minnreduction = eco_value['nreductionmin']
          eco_producer.maxnreduction = eco_value['nreductionmax']
          eco_producer.nh4nreduction = eco_value['nreductionnh4']
          eco_producer.no3nreduction = eco_value['nreductionno3']
          eco_producer.minpreduction = eco_value['preductionmin']
          eco_producer.maxpreduction = eco_value['preductionmax']
          eco_producer.nh4preduction = eco_value['preductiontotalp']
          eco_producer.no3preduction = eco_value['preductionsolublep']
          
          if image_data['status'] == 'success'
            image_url = image_data['data']
            file = MiniMagick::Image.open(URI.decode(image_url['imageurl']))
            save_to_local = file.write  "public/assets/images/#{eco_value['id']}.#{file.type}"
            eco_producer.image = "#{eco_value['id']}.#{file.type}"
          end
          if eco_value['practiceTypeID'] == 1
            eco_producer.impact = 'Water Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 2 || eco_value['practiceTypeID'] == 3
            eco_producer.impact = 'Water Quality,Air Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 4 || eco_value['practiceTypeID'] == 5
            eco_producer.impact = 'Water Quality'
          elsif eco_value['practiceTypeID'] == 6
            eco_producer.impact = 'Water Quality,Erosion Control,Carbon Reduction'
          elsif eco_value['practiceTypeID'] == 7
            eco_producer.impact = 'Water Quality,Air Quality'
          elsif eco_value['practiceTypeID'] == 8
            eco_producer.impact = 'Water Quality'                      
          end
          eco_producer.save
      end
    end   
  end

  def import_practice_api
    impact_data  = JSON.parse(open('https://producer.ecocredits.me/api/v-1-3/practice-types/get.php?
token=yi9u/BzRryjjTAafHh4ljkUvDzAMwPhQ7MTCFb2UNuE=').read)
    impact_data  = JSON.parse(impact_data['data'])

    impact_data.each do |impact_value|
      if EchoPracticeApi.exists?(:practice_id => impact_value['id']) == false
        eco_practice = EchoPracticeApi.new
        eco_practice.practice_id = impact_value['id']
        eco_practice.name = impact_value['name']
        eco_practice.nreduction = impact_value['nreduction']
        eco_practice.preduction = impact_value['preduction']
        eco_practice.description = impact_value['description']
        impact = ''
        JSON.parse(impact_value['environmentalImpacts']).each do |value|
          impact = impact.to_s + value['name'] + ','
        end
        eco_practice.environmentalImpacts = impact.chop
        eco_practice.save
      end
    end

  end


  def search_results    
    producer_id = params[:cert]
   

    @producer = EchoProducerApi.where(producer_id: producer_id)
    
    @Soxx_ref      = 'Federal Specifications for Compost Filter Socks for Sediment & Erosion Control: 
                     <a href="http://filtrexxns.com/wp-content/uploads/2013/04/Filtrexx-Federal-Aproval-Boo.pdf" target="_blank">http://filtrexxns.com/wp-content/uploads/2013/04/Filtrexx-Federal-Aproval-Boo.pdf</a>'

    @Inhibitor_ref = 'IDALS, IDNR, & ISU CALS, 2014: 
                          <a target="_blank" href="http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf">http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf</a>

                          USDA NRCS, 2014: 
                          <a target="_blank" href="http://www.nrcs.usda.gov/wps/PA_NRCSConsumption/download?cid=stelprdb1255636&ext=pdf">http://www.nrcs.usda.gov/wps/PA_NRCSConsumption/download?cid=stelprdb1255636&ext=pdf</a>'

    @Notill_ref    = 'IDALS, IDNR, & ISU CALS, 2014: 
                       <a target="_blank" href="http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf">http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf</a>

                      USDA NRCS 2013: 
                      <a href="http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1249901.pdf" target="_blank">http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1249901.pdf</a>'

    @Tillage_ref   = 'IDALS, IDNR, & ISU CALS, 2014: 
                    <a target="_blank" href="http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf">http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf</a>

                    USDA NRCS, 2013: 
                    <a target="_blank" href="http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1251402.pdf">http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1251402.pdf</a>'

    @Cover_ref     = 'USDA NRCS, 2014: 
                    <a href="http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1263176.pdf" target="_blank">http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1263176.pdf</a>'

    @Buffer_ref    = 'Helmers, Isenhart, Dosskey, Dabney, & Strock 2006: 
                    <a href="http://www.epa.gov/sites/production/files/2015-07/documents/2006_8_24_msbasin_symposia_ia_session4-2.pdf" target="_blank">http://www.epa.gov/sites/production/files/2015-07/documents/2006_8_24_msbasin_symposia_ia_session4-2.pdf</a>

                    USDA NRCS, 2013: 
                    <a target = "_blank" href="http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1241319.pdf">http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1241319.pdf</a>'

    @Bioreactor_ref = 'DALS, IDNR, & ISU CALS, 2014: 
                      <a href="http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf" target="_blank">http://www.nutrientstrategy.iastate.edu/sites/default/files/documents/NRS2-141001.pdf</a>

                      USDA NRCS, 2015: 
                      <a target="_blank" href="http://www.nrcs.usda.gov/wps/PA_NRCSConsumption/download?cid=nrcseprd340747&ext=pdf">http://www.nrcs.usda.gov/wps/PA_NRCSConsumption/download?cid=nrcseprd340747&ext=pdf</a>'
    
    @Split_ref = '4R Nutrient Stewardship, 2016 
                  <a href="http://www.nutrientstewardship.com/implement-4rs/article/split-fertilizer-application-helps-optimize-nutrient-management" target="_blank">http://www.nutrientstewardship.com/implement-4rs/article/split-fertilizer-application-helps-optimize-nutrient-management</a>

                  USDA NRCS, 2012 
                  <a href="http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1046896.pdf" target="_blank">http://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb1046896.pdf</a>'
      @producer.each do |ref_value|        
          if ref_value.practiceTypeID == '1'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Cover_ref
          elsif ref_value.practiceTypeID == '2'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Notill_ref
          elsif ref_value.practiceTypeID == '3'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Tillage_ref
          elsif ref_value.practiceTypeID == '4'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Buffer_ref
          elsif ref_value.practiceTypeID == '5'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Bioreactor_ref      
          elsif ref_value.practiceTypeID == '6'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Soxx_ref
          elsif ref_value.practiceTypeID == '7'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Inhibitor_ref
          elsif ref_value.practiceTypeID == '8'
            @producer_content = EchoPracticeApi.where(practiceTypeID: ref_value.practiceTypeID)
            @producer_content.each do |content|
              @content_detail = content.description
            end
            @ref_details = @Split_ref
          end
        end
  end

  def check_echotag
    @data = EchoProducerApi.exists?(:producer_id => params[:cert])
    render json: @data
  end

  def echotag_enq
      check_captcha(params, request) if request
      if @recaptcha_response['success'] != true
        "<h4>CAPTCHA mismatch, try again</h4>"
      elsif ImportApi.enquery_ecotag(params[:name], params[:email], params[:phone], params[:subject], params[:message], params[:echotag_id]).deliver
        render json: true
      else
        render json: false
      end
  end

  def check_captcha(params, request)
    secret = "6LfutQ0UAAAAAAZv9Tq0SUCBnUFQrR8r8QhvzzQO"
    uri = URI.parse("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + params['g-recaptcha-response'] + "")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request_url = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request_url)
    @recaptcha_response =  JSON.parse(response.body)    
  end
  
end
