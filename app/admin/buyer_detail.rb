ActiveAdmin.register BuyerDetail do

  permit_params :company_name, :company_contact_name, :company_contact_email, :company_contact_phone, :company_contact_address, :company_contact_city, :company_contact_state, :company_contact_zip, :company_logo,
                :company_header, :password, :confirm_password, :purchase_date, :is_active

end
