class ImportApi < ApplicationMailer

  def import_email(id)
  	@id = id
    mail(to: "kristin@r5i.com", from: "import@ecopractice.com", subject: 'Redstone - Production API Data Import')
  end

  def enquery_ecotag(name, phone, email, subject, message, ecotagid)
  	@name = name
  	@phone = phone
  	@email = email
  	@message = message
  	@ecotagid = ecotagid
  	mail(to: "doug@agsolver.com,jharsch@sustainableenviro.com,bob@r5i.com", from: "inquiry@ecopractices.com", subject: "Ecotag Inquiry")
  end
  
end
