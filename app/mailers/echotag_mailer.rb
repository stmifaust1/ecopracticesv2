class EchotagMailer < ApplicationMailer
	default :from => "EcoPractices <info@ecopractices.com>"

 def ecotag_email(name, email, phone , subject, message)
    @name = name
    @email = email
    @phone = phone
    @subject = subject
    @message = message
    mail(:to => 'doug@agsolver.com,jharsch@sustainableenviro.com,bob@r5i.com,JJensen@ecopractices.com', :subject => @subject)
  end
end
