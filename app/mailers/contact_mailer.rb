class ContactMailer < ActionMailer::Base

  def contact_email(message)
    @message = message
    mail(to: "clhiemstra@gmail.com,JJensen@ecopractices.com", from: @message.email, subject: 'Redstone - Contact Message')
  end
end
