require 'open-uri'
require 'openssl'
require 'mini_magick'

class EchoProducerApi < ActiveRecord::Base

def self.import_producer_api
    data_main  = JSON.parse(open('https://producer.ecocredits.me/api/v-1-4/credits/get.php?token=yi9u/BzRryjjTAafHh4ljkUvDzAMwPhQ7MTCFb2UNuE=').read)
    @new_record = JSON.parse(data_main['data'])    

    @new_record.each do |eco_value|
      eco_producer = EchoProducerApi.new      
      eco_producer_update = EchoProducerApi.find_by(producer_id: eco_value['id'])

      url = "https://producer.ecocredits.me/api/v-1-4/credits/getImage.php?token=yi9u/BzRryjjTAafHh4ljkUvDzAMwPhQ7MTCFb2UNuE=#{eco_value['id']}"
      image_data = JSON.parse(open(url).read)

      if EchoProducerApi.exists?(:producer_id => eco_value['id'])
        eco_producer_update.value = eco_value['value']
        if eco_value['quantity']
          eco_producer_update.quantity = eco_value['quantity']
        else
          eco_producer_update.quantity = 0
        end
          eco_producer_update.startDate = eco_value['startDate']
          eco_producer_update.nreduction = eco_value['nreductionavg'] 
          eco_producer_update.preduction = eco_value['preductionavg']
          eco_producer_update.feet = eco_value['feet']
          eco_producer_update.approvalDate = eco_value['approvalDate']
          eco_producer_update.acres = eco_value['acres']
          eco_producer_update.practiceTypeID = eco_value['practiceTypeID']
          eco_producer_update.practiceTypeName = eco_value['practiceTypeName']
          eco_producer_update.feetRequired = eco_value['feetRequired']
          eco_producer_update.stateID = eco_value['stateID']
          eco_producer_update.stateName = eco_value['stateName']
          eco_producer_update.minnreduction = eco_value['nreductionmin']
          eco_producer_update.maxnreduction = eco_value['nreductionmax']
          eco_producer_update.nh4nreduction = eco_value['nreductionnh4']
          eco_producer_update.no3nreduction = eco_value['nreductionno3']
          eco_producer_update.minpreduction = eco_value['preductionmin']
          eco_producer_update.maxpreduction = eco_value['preductionmax']
          eco_producer_update.nh4preduction = eco_value['preductiontotalp']
          eco_producer_update.no3preduction = eco_value['preductionsolublep']

          if !eco_producer_update.image
            if image_data['status'] == 'success'
              image_url = image_data['data']
              file = MiniMagick::Image.open(URI.decode(image_url['imageurl']))
              save_to_local = file.write  "public/images/#{eco_value['id']}.#{file.type}"
              eco_producer_update.image = "#{eco_value['id']}.#{file.type}"
            end
          end
          if eco_value['practiceTypeID'] == 1
            eco_producer_update.impact = 'Water Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 2 || eco_value['practiceTypeID'] == 3
            eco_producer_update.impact = 'Water Quality,Air Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 4 || eco_value['practiceTypeID'] == 5
            eco_producer_update.impact = 'Water Quality'
          elsif eco_value['practiceTypeID'] == 6
            eco_producer_update.impact = 'Water Quality,Erosion Control,Carbon Reduction'
          elsif eco_value['practiceTypeID'] == 7
            eco_producer_update.impact = 'Water Quality,Air Quality'
          elsif eco_value['practiceTypeID'] == 8
            eco_producer_update.impact = 'Water Quality'    
          end
          eco_producer_update.save
      else
          eco_producer.producer_id = eco_value['id']
          eco_producer.value = eco_value['value']
          if eco_value['quantity']
            eco_producer.quantity = eco_value['quantity']
          else
            eco_producer.quantity = 0
          end
          eco_producer.startDate = eco_value['startDate']        
          eco_producer.nreduction = eco_value['nreductionavg']
          eco_producer.preduction = eco_value['preductionavg']
          eco_producer.feet = eco_value['feet']
          eco_producer.approvalDate = eco_value['approvalDate']
          eco_producer.acres = eco_value['acres']
          eco_producer.practiceTypeID = eco_value['practiceTypeID']
          eco_producer.practiceTypeName = eco_value['practiceTypeName']
          eco_producer.feetRequired = eco_value['feetRequired']
          eco_producer.stateID = eco_value['stateID']
          eco_producer.stateName = eco_value['stateName']
          eco_producer.minnreduction = eco_value['nreductionmin']
          eco_producer.maxnreduction = eco_value['nreductionmax']
          eco_producer.nh4nreduction = eco_value['nreductionnh4']
          eco_producer.no3nreduction = eco_value['nreductionno3']
          eco_producer.minpreduction = eco_value['preductionmin']
          eco_producer.maxpreduction = eco_value['preductionmax']
          eco_producer.nh4preduction = eco_value['preductiontotalp']
          eco_producer.no3preduction = eco_value['preductionsolublep']
          
          if image_data['status'] == 'success'
            image_url = image_data['data']
            file = MiniMagick::Image.open(URI.decode(image_url['imageurl']))
            save_to_local = file.write  "public/images/#{eco_value['id']}.#{file.type}"
            eco_producer.image = "#{eco_value['id']}.#{file.type}"
          end
          if eco_value['practiceTypeID'] == 1
            eco_producer.impact = 'Water Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 2 || eco_value['practiceTypeID'] == 3
            eco_producer.impact = 'Water Quality,Air Quality,Erosion Control'
          elsif eco_value['practiceTypeID'] == 4 || eco_value['practiceTypeID'] == 5
            eco_producer.impact = 'Water Quality'
          elsif eco_value['practiceTypeID'] == 6
            eco_producer.impact = 'Water Quality,Erosion Control,Carbon Reduction'
          elsif eco_value['practiceTypeID'] == 7
            eco_producer.impact = 'Water Quality,Air Quality'
          elsif eco_value['practiceTypeID'] == 8
            eco_producer.impact = 'Water Quality'                      
          end
          eco_producer.save
          ImportApi.import_email(eco_value['id']).deliver
      end
    end   
  end

end